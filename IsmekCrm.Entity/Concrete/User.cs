﻿using IsmekCrm.Core.Entity;
using System.ComponentModel.DataAnnotations;

namespace IsmekCrm.Entity.Concrete
{
    public class User:IEntity
    {
       [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(20, ErrorMessage ="Username must be 5-20 characters.", MinimumLength =5)]
        public string Username { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [StringLength(20, ErrorMessage = "Username must be 5-20 characters.", MinimumLength = 5)]
        public string Password { get; set; }



        //Navigation properties for department

    }
}
